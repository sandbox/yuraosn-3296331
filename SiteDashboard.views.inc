<?php

/**
 * Implements hook_views_data_alter().
 */
function site_dashboard_views_data_alter(array &$data) {
  $data['node']['service_views_filter'] = [
    'title' => t('Service filter'),
    'filter' => [
      'title' => t('Service filter'),
      'help' => t('Provides a custom filter for job.'),
      'field' => 'nid',
      'id' => 'service_views_filter',
    ],
  ];

  $data['node']['job_request_counter'] = [
    'title' => t('Job request counter'),
    'field' => [
      'title' => t('Job request counter'),
      'help' => t('The number of applications sent for the vacancy.'),
      'id' => 'job_request_counter',
    ],
  ];

  $data['webform_submission']['request_ajax_field'] = [
    'title' => t('Request Ajax Field'),
    'field' => [
      'title' => t('Request Ajax Field'),
      'help' => t('Fields for ajax changes: notes, sticky, locked.'),
      'id' => 'request_ajax_field',
    ],
  ];
}
