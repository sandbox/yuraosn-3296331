<?php

namespace Drupal\site_dashboard\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\site_dashboard\Helper\SiteDashboardHelper;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filters by service.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("service_views_filter")
 */
class ServiceViewsFilter extends ManyToOne {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Filter by service');
    $this->definition['options callback'] = [$this, 'getServices'];
    $this->currentDisplay = $view->current_display;
  }

  /**
   * Helper function that generates the options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @return array|mixed
   *   An array of services.
   */
  public function getServices() {
    $options = [
      'current_user' => t('From current user'),
    ];
    $services = SiteDashboardHelper::getServices();
    if ($services) {
      $options += $services;
    }

    return $options;
  }

  /**
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   */
  public function operators() {
    $operators = parent::operators();

    // @TODO In the future, we will expand the operator.
    return [
      'or' => $operators['or'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value']['#multiple'] = FALSE;
    unset($form['value']['#options']['all']);
  }

  /**
   * Helper function that builds the query.
   */
  public function query() {

    // Skip validate for administrator.
    if (SiteDashboardHelper::getUserType('administrator')) {
      return;
    }

    // Filter for specific tables.
    $tables_available = [
      'webform_submission',
      'node_field_data',
    ];
    $base_table = $this->view->storage->get('base_table');
    if (!in_array($base_table, $tables_available)) {
      $this->messenger()
        ->addMessage($this->t('This "Views" does not support filtering: "@filter_name".', [
          '@filter_name' => t('Filter by service'),
        ]), 'warning');

      return;
    }

    $service_tid = $this->getFilterServiceId();
    $node_field_name = 'node__' . KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE;
    $configuration = [
      'table' => $node_field_name,
      'field' => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'operator' => '=',
    ];
    $join = Views::pluginManager('join')
      ->createInstance('standard', $configuration);

    $link_point = NULL;
    switch ($base_table) {
      case 'webform_submission':
        if (!isset($this->query->tables['webform_submission']['webform_submission_field_application_for_job_job'])) {
          $this->messenger()
            ->addMessage($this->t('This "Views" does not support filtering: "@filter_name".', [
              '@filter_name' => t('Filter by service'),
            ]), 'warning');

          return;
        }
        $link_point = 'node_field_data_webform_submission_field_application_for_job_job';
        break;
    }

    $this->query->addRelationship($node_field_name, $join, 'node_field_data', $link_point);
    $this->query->addWhere('AND', $node_field_name . '.' . KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE . '_target_id', $service_tid, '=');
  }

  /**
   * Helper function that get ServiceId.
   *
   * @return int
   *   Taxonomy term ID.
   */
  public function getFilterServiceId() {
    $service_tid = FALSE;
    if (!empty($this->value)) {
      $service_tid = $this->value;
      if ($service_tid == 'current_user') {
        $service_tid = SiteDashboardHelper::getServiceByUser();
      }
    }

    return (int) $service_tid;
  }

}
