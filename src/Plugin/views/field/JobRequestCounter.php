<?php

/**
 * @file
 * Definition of
 * Drupal\site_dashboard\Plugin\views\field\JobRequestCounter
 */

namespace Drupal\site_dashboard\Plugin\views\field;

use Drupal;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use PDO;

/**
 * Field handler for the number of applications for the vacancy.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("job_request_counter")
 */
class JobRequestCounter extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $values->_entity;
    $request_count_all = $request_count_unread = 0;
    $nid = $node->id();
    $query = Drupal::service('database')->select('webform_submission_data', 'wsd');
    $query->innerJoin('webform_submission', 'ws', 'wsd.sid = ws.sid');
    $query->fields('wsd', ['sid'])
      ->condition('wsd.webform_id', 'application_for_job')
      ->condition('ws.webform_id', 'application_for_job')
      ->condition('wsd.name', 'job')
      ->condition('wsd.value', $nid);
    $results = $query->execute()->fetchAll(PDO::FETCH_COLUMN);

    if ($results) {
      $request_count_all = count($results);

      $query = Drupal::service('database')
        ->select('webform_submission_data', 'wsd')
        ->fields('wsd', ['sid'])
        ->orderBy('wsd.sid', 'DESC')
        ->condition('wsd.name', 'status')
        ->condition('wsd.sid', $results, 'IN');
      $or = new Condition('OR');
      $or->condition('wsd.value', '');
      $or->condition('wsd.value', KRUSCHINA_DASHBOARD_STATUS_NEW_TID);
      $query->condition($or);

      $select = $query->execute();
      $results = $select->fetchAll(PDO::FETCH_COLUMN);
      if ($results) {
        $request_count_unread = count($results);
      }
    }

    $filter_job = $node->label() . ' (' . $nid . ')';
    $output = '';
    if ($request_count_all) {
      $link = Link::fromTextAndUrl($request_count_all, Url::fromUri('base:admin/dashboard/request', [
        'query' => ['job' => $filter_job],
        'attributes' => [
          'title' => $this->t('Alle Anwendungen'),
          'class' => ['item all'],
        ],
      ]));
      $link = $link->toRenderable();
      $output .= render($link);
    }
    else {
      $output .= '<div class="item all">0</div>';
    }
    $output .= ' / ';
    if ($request_count_unread) {
      $link = Link::fromTextAndUrl($request_count_unread, Url::fromUri('base:admin/dashboard/request', [
        'query' => [
          'job' => $filter_job,
          'webform_submission_value_2' => 'none',
        ],
        'attributes' => [
          'title' => $this->t('Unverarbeitete Anwendungen'),
          'class' => ['item new'],
        ],
      ]));
      $link = $link->toRenderable();
      $output .= render($link);
    }
    else {
      $output .= '<div class="item new">0</div>';
    }

    return Markup::create($output);
  }

}
