<?php

/**
 * @file
 * Definition of
 * Drupal\site_dashboard\Plugin\views\field\RequestAjaxField
 */

namespace Drupal\site_dashboard\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\site_dashboard\Helper\SiteDashboardRequestHelper;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Fields for ajax changes: notes, sticky, locked.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("request_ajax_field")
 */
class RequestAjaxField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['field_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Ajax field'),
      '#options' => [
        'sticky' => $this->t('Sticky'),
        'locked' => $this->t('Locked'),
        'notes' => $this->t('Notes'),
      ],
      '#default_value' => $this->options['field_type'],
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    /** @var $entity \Drupal\webform\Entity\WebformSubmission */
    $entity = $values->_entity;
    $field_type = !empty($this->options['field_type']) ? $this->options['field_type'] : NULL;
    $render = SiteDashboardRequestHelper::actionsAjaxLinkBuild($entity, $field_type);

    return $render ? $render : '';

  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['field_type'] = ['default' => 'sticky'];

    return $options;
  }

}
