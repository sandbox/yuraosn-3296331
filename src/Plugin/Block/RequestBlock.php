<?php

namespace Drupal\site_dashboard\Plugin\Block;

use Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block to a field on an entity.
 *
 * @Block(
 *   id = "tandem_dashboard_request_block_view",
 *   admin_label = @Translation("Dashboard request view"),
 *   category = @Translation("Dashboard")
 * )
 */
class RequestBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = $this->buildViewRequest();

    return $build;
  }

  /**
   * Build block for edit request.
   *
   * @return array
   *   Build content.
   */
  public function buildViewRequest() {
    $request = Drupal::request();

    /** @var $webform_submission \Drupal\webform\Entity\WebformSubmission */
    $webform_submission = $request->attributes->get('webform_submission');
    $build = Drupal::formBuilder()
      ->getForm('\Drupal\site_dashboard\Form\RequestForm', $webform_submission);

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'site_dashboard access manager');
  }

}
