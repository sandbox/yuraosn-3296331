<?php

namespace Drupal\site_dashboard\Helper;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Helper functions for Site of module.
 *
 * Class SiteDashboardMailHelper.
 *
 * @package Drupal\site_dashboard\Helper
 */
class SiteDashboardMailHelper extends SiteDashboardHelper {

  /**
   * Send mail to users.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $action
   *   Insert, update.
   */
  public function mailSend(EntityInterface $entity, $action = '') {

    /** @var array $actions */
    $actions = $this->mailGetAction($entity, $action);
    if (!$actions) {
      return;
    }

    foreach ($actions as $action_name => $params) {
      $template = $this->mailGetTemplate($action_name, $params['tokens']);
      if (!$template) {
        return;
      }

      foreach ($params['recipients'] as $recipient) {

        if (is_string($recipient)) {

          /** @var string $recipient - email */
          $to = $recipient;
          $lang = 'de';
        }
        else {

          /** @var \Drupal\user\Entity\User $recipient */
          $to = $recipient->getEmail();
          $lang = $recipient->language()->getId();
        }

        $from = Drupal::config('system.site')->get('mail');
        $params_mail = [
          'subject' => $template['subject'],
          'message' => $template['message'],
        ];
        $result = Drupal::service('plugin.manager.mail')
          ->mail('site_dashboard', 'dashboard_notify', $to, $lang, $params_mail, $from, TRUE);

        if ($result['result'] !== TRUE) {
          Drupal::logger('site_dashboard')
            ->error(t('There was a problem sending your message and it was not sent. File: @file', ['@file' => __FILE__ . ':' . __LINE__]));
        }
      }
    }
  }

  /**
   * Get action for mail.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $action
   *   Available: insert, update.
   *
   * @return array|void
   */
  private function mailGetAction(EntityInterface $entity, $action = '') {
    $actions = [];
    if (!in_array($entity->getEntityTypeId(), ['node', 'webform_submission'])) {
      return;
    }

    /** @var \Drupal\node\Entity\Node|\Drupal\webform\Entity\WebformSubmission $entity */
    $tokens = [];

    if ($entity->getEntityTypeId() == 'node'
      && $entity->bundle() == 'job_directory'
    ) {
      /** @var \Drupal\node\Entity\Node $entity */

      $owner = $entity->getOwner();
      $tokens['user'] = $owner;
      $tokens['node'] = $entity;
      $current_user_is_manager = self::getUserType('manager');
      $author_is_manager = self::getUserType('manager', $owner->id());
      $top_managers = self::jobGetTopManagers($entity);

      // Node insert.
      if ($action == 'insert') {
        if ($current_user_is_manager) {

          /*
           * Send to Manager.
           *
           * The vacancy has been created and sent
           * for moderation for publication.
           */
          $actions['db_man_job_new'] = [
            'recipients' => [$owner],
            'tokens' => $tokens,
          ];

          /*
           * Send to Top-manager.
           *
           * Vacancy created (for publication)
           */
          $actions['db_topman_job_new'] = [
            'recipients' => $top_managers,
            'tokens' => $tokens,
          ];
        }
        elseif ($author_is_manager) {

          /*
           * Send to Manager.
           *
           * Change in responsible (you became responsible)
           */
          $actions['db_man_job_resp'] = [
            'recipients' => [$owner],
            'tokens' => $tokens,
          ];
        }
      }

      // Node update.
      else {
        if ($entity->isPublished()
          && (empty($entity->original) || !$entity->original->isPublished())
        ) {

          /*
           * Send to Manager.
           *
           * Job posted.
           */
          $actions['db_man_job_published'] = [
            'recipients' => [$owner],
            'tokens' => $tokens,
          ];
        }
        if ($author_is_manager && $current_user_is_manager) {

          /*
           * Send to Top-manager.
           *
           * Vacancy updated (for publication)
           */
          $actions['db_topman_job_updated'] = [
            'recipients' => $top_managers,
            'tokens' => $tokens,
          ];
        }
        if ($entity->getOwnerId() <> $entity->original->getOwnerId()) {
          if ($entity->getOwnerId()) {

            /*
             * Send to Manager.
             *
             * Change in responsible (you became responsible)
             */
            $actions['db_man_job_resp'] = [
              'recipients' => [$owner],
              'tokens' => $tokens,
            ];
          }

          if ($entity->original->getOwnerId()) {
            $owner_old = User::load($entity->original->getOwnerId());

            /*
             * Send to Manager.
             *
             * Change of responsible (you are no longer responsible)
             */
            $actions['db_man_job_not_resp'] = [
              'recipients' => [$owner_old],
              'tokens' => [
                  'user' => $owner_old,
                ] + $tokens,
            ];
          }
        }
      }
    }

    // Webform Submissions.
    elseif ($entity->getEntityTypeId() == 'webform_submission'
      && $entity->bundle() == 'application_for_job'
    ) {

      /** @var \Drupal\webform\Entity\WebformSubmission $entity */

      $data = $entity->getData();
      if (empty($data['job'])) {
        return;
      }

      /** @var \Drupal\node\Entity\Node $job_node */
      $job_node = Node::load($data['job']);
      $tokens['webform_submission'] = $entity;
      $tokens['node'] = $job_node;

      /*
       * Send to Client.
       *
       * Notification of the client about the creation or change
       * of the status of the application.
       */
      $data_old = isset($entity->original) ? $entity->original->getData() : [];
      if (!empty($data['status']) && $data['status'] <> $data_old['status']) {
        $actions['db_cl_request_status'] = [
          'recipients' => [$data['e_mail_adresse']],
          'tokens' => $tokens,
        ];
      }

      /*
       * Send to Manager.
       *
       * A new job application has been created
       */
      if ($action == 'insert') {
        $actions['db_man_request_new'] = [
          'recipients' => [$job_node->getOwner()],
          'tokens' => $tokens,
        ];
      }
    }

    return $actions;
  }

  /**
   * Get template for mail.
   *
   * @param string $action_name
   *   The action name.
   * @param array $tokens
   *   Array tokens for replace in text.
   *
   * @return array|bool
   *   Return array:
   *   - 'subject' => $subject,
   *   - 'message' => $message,
   */
  private function mailGetTemplate(string $action_name, array $tokens = []) {
    $field_name = 'field_p_' . $action_name;
    $config_page = config_pages_config('dashboard');
    if (!$config_page
      || !$config_page->hasField($field_name)
      || $config_page->get($field_name)->isEmpty()
    ) {
      return FALSE;
    }

    /** @var null|\Drupal\paragraphs\Entity\Paragraph $p_config */
    $p_config = NULL;

    // Get template fro request status.
    if ($action_name == 'db_cl_request_status') {
      if (empty($tokens['webform_submission'])) {
        return FALSE;
      }

      /** @var \Drupal\webform\Entity\WebformSubmission $entity */
      $entity = $tokens['webform_submission'];

      $data = $entity->getData();
      $status = $data['status'];
      foreach ($config_page->get($field_name) as $p_config_item) {
        $p_config_item = $p_config_item->entity;
        if ($p_config_item->hasField('field_mail_status')
          && !$p_config_item->get('field_mail_status')->isEmpty()
          && $p_config_item->get('field_mail_status')->getString() == $status
        ) {

          // Get only first template, other template skip.
          $p_config = $p_config_item;
          break;
        }
      }
    }
    else {
      $p_config = $config_page->get($field_name)->entity;
    }

    if ($p_config
      && $p_config->hasField('field_mail_subject')
      && !$p_config->get('field_mail_subject')->isEmpty()
      && $p_config->hasField('field_mail_message')
      && !$p_config->get('field_mail_message')->isEmpty()
    ) {
      $subject = $p_config->get('field_mail_subject')->value;
      if ($subject) {
        $subject = Drupal::token()->replace($subject, $tokens);
      }
      $message = $p_config->get('field_mail_message')->value;
      if ($message) {
        $message = Drupal::token()->replace($message, $tokens);
      }

      if ($subject && $message) {

        return [
          'subject' => $subject,
          'message' => $message,
        ];
      }
    }

    return FALSE;
  }

}
