<?php

namespace Drupal\site_dashboard\Helper;

use Drupal;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Controller\WebformSubmissionController;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Utility\WebformDateHelper;
use Drupal\webform\Utility\WebformDialogHelper;
use PDO;

/**
 * Helper functions for Site of module.
 *
 * Class SiteDashboardRequestHelper
 *
 * @package Drupal\site_dashboard\Helper
 */
class SiteDashboardRequestHelper extends SiteDashboardHelper {

  /**
   * Checking email for uniqueness for applications with the status of "New".
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @return bool
   */
  public static function actionsValidateEmailUnique(&$form, FormStateInterface &$form_state) {

    // Only for create webform_submission, not edit.
    if (isset($form['navigation']['#webform_submission'])) {
      return FALSE;
    }

    $values = $form_state->getUserInput();
    if (!empty($values['e_mail_adresse'])) {
      $email = $values['e_mail_adresse'];

      $select = Drupal::service('database')
        ->select('webform_submission_data', 'wsd')
        ->fields('wsd', ['sid'])
        ->orderBy('wsd.sid', 'DESC')
        ->condition('wsd.webform_id', 'application_for_job')
        ->condition('wsd.name', 'e_mail_adresse')
        ->condition('wsd.value', $email)
        ->execute();

      $results = $select->fetchAll(PDO::FETCH_COLUMN);
      if ($results) {
        $storage = Drupal::entityTypeManager()
          ->getStorage('webform_submission');
        $submissions = $storage->loadMultiple($results);
        foreach ($submissions as $submission) {

          /** @var \Drupal\webform\Entity\WebformSubmission $submission */

          $data = $submission->getData();
          if (isset($data['e_mail_adresse'])
            && $data['e_mail_adresse'] == $email
            && in_array($data['status'], [KRUSCHINA_DASHBOARD_STATUS_CLOSE_TID, KRUSCHINA_DASHBOARD_STATUS_NEW_TID])
          ) {
            $form_state->setErrorByName('e_mail_adresse', t('Mit der E-Mail %mail wurde bereits eine Bewerbung gesendet, die Sie nicht erneut senden können.', [
              '%mail' => $email,
            ]));
            return FALSE;
          }
        };
      }
    }

    return TRUE;
  }

  /**
   * Block action links for request submit.
   *
   * @param \Drupal\webform\Entity\WebformSubmission $webform_submission
   * @param string $type
   *  Available: view, edit.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @return array
   */
  public static function actionsFormBuild(WebformSubmission $webform_submission, $type = 'view') {
    $form = [];
    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('Actions'),
    ];
    $job = $webform_submission->getElementData('job');
    if ($job) {
      $storage = Drupal::entityTypeManager()->getStorage('node');
      $node = $storage->load($job);
      if ($node) {
        $link = $node->toLink($node->label())->toRenderable();
        $link = Drupal::service('renderer')->render($link);
        $details = t('Created') . ': ' . WebformDateHelper::format($webform_submission->getCreatedTime())
          . '<br>' . t('Job') . ': ' . $link;

        $form['wrapper']['information'] = [
          '#type' => 'item',
          '#markup' => $details,
        ];
      }
    }

    $actions = [
      'notes',
      'sticky',
      'locked',
    ];
    $render = self::actionsAjaxLinkBuild($webform_submission, $actions);
    $form['wrapper']['action_links'] = [
      '#type' => 'item',
      '#title' => t('Action Links'),
      '#markup' => $render,
      '#weight' => 10,
    ];
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['#weight'] = 9;

    switch ($type) {
      case 'view':
        $form['wrapper']['actions']['view'] = [
          '#type' => 'link',
          '#title' => t('View request'),
          '#url' => $webform_submission->toUrl(),
        ];
        break;

      case 'edit':
        $route_name = 'entity.webform_submission.edit_form';
        $route_parameters = [
          'webform' => $webform_submission->getWebform()->id(),
          'webform_submission' => $webform_submission->id(),
        ];
        $request = Drupal::request();
        $destination = $request->query->get('destination');
        $form['wrapper']['actions']['edit'] = [
          '#type' => 'link',
          '#title' => t('Edit'),
          '#url' => Url::fromRoute($route_name, $route_parameters, ['query' => ['destination' => $destination]]),
        ];
        break;
    }

    if ($webform_submission->access('delete')) {

      /** @var \Drupal\webform\WebformRequestInterface $request_handler */
      $request_handler = Drupal::service('webform.request');
      $base_route_name = 'webform_submission.delete_form';
      if (strpos(Drupal::routeMatch()->getRouteName(), 'webform.user.submission') !== FALSE) {
        $base_route_name = 'webform.user.submission.delete';
      }
      $url = $request_handler->getUrl($webform_submission, $webform_submission, $base_route_name);
      $form['wrapper']['actions']['delete'] = [
        '#type' => 'link',
        '#title' => t('Delete submission'),
        '#url' => $url,
        '#attributes' => WebformDialogHelper::getModalDialogAttributes(WebformDialogHelper::DIALOG_NARROW, [
          'button',
          'button--danger',
        ]),
      ];
    }

    return $form;
  }

  /**
   * Render ajax link action for request.
   *
   * @param \Drupal\webform\Entity\WebformSubmission $entity
   *
   * @param array $actions
   *   Available:
   *   - notes
   *   - sticky
   *   - locked.
   *
   * @return array|string
   */
  public static function actionsAjaxLinkBuild(WebformSubmission $entity, $actions = []) {
    $render = [];

    // Build link for notes.
    if (in_array('notes', $actions)) {

      /**
       * @see \Drupal\webform\WebformSubmissionListBuilder::buildRowColumn()
       */
      $route_parameters = [
        'webform' => $entity->getWebform()->id(),
        'webform_submission' => $entity->id(),
      ];
      $destination = Drupal::destination()->get();
      if ($destination) {
        $route_parameters['destination'] = $destination;
      }
      $route_name = 'entity.webform_submission.notes_form';
      $state = $entity->get('notes')->value ? 'on' : 'off';
      $t_args = ['@label' => $entity->label()];
      $label = $entity->get('notes')->value ? t('Edit @label notes', $t_args) : t('Add notes to @label', $t_args);
      $render_item = [
        '#type' => 'link',
        '#title' => new FormattableMarkup('<span class="webform-icon webform-icon-notes webform-icon-notes--@state"></span><span class="visually-hidden">@label</span>', [
          '@state' => $state,
          '@label' => $label,
        ]),
        '#url' => Url::fromRoute($route_name, $route_parameters),
        '#attributes' => WebformDialogHelper::getModalDialogAttributes(WebformDialogHelper::DIALOG_NARROW),
      ];
      $render_item['#attached']['library'][] = 'webform/webform.ajax';
      $render_item['#attributes']['title'] = t('Notes');
      $notes = $entity->getNotes();
      if ($notes) {
        $render_item['#attributes']['title'] = $notes;
      }
      $render[] = $render_item;
    }

    // Build link for sticky.
    if (in_array('sticky', $actions)) {

      /**
       * @see \Drupal\webform\Controller\WebformSubmissionController::sticky
       */
      $route_name = 'entity.webform_submission.sticky_toggle';
      $route_parameters = [
        'webform' => $entity->getWebform()->id(),
        'webform_submission' => $entity->id(),
      ];
      $render[] = [
        '#type' => 'link',
        '#title' => WebformSubmissionController::buildSticky($entity),
        '#url' => Url::fromRoute($route_name, $route_parameters),
        '#attributes' => [
          'id' => 'webform-submission-' . $entity->id() . '-sticky',
          'title' => t('Sticky'),
          'class' => ['use-ajax'],
        ],
      ];
    }

    // Build link for locked.
    if (in_array('locked', $actions)) {

      /**
       * @see \Drupal\webform\Controller\WebformSubmissionController::locked
       */
      $route_name = 'entity.webform_submission.locked_toggle';
      $route_parameters = [
        'webform' => $entity->getWebform()->id(),
        'webform_submission' => $entity->id(),
      ];
      if (SiteDashboardHelper::getUserType('top_manager')
        || SiteDashboardHelper::getUserType('administrator')
      ) {
        $title = $entity->isLocked() ? t('Locked') : t('Unlocked');
        $render[] = [
          '#type' => 'link',
          '#title' => WebformSubmissionController::buildLocked($entity),
          '#url' => Url::fromRoute($route_name, $route_parameters),
          '#attributes' => [
            'id' => 'webform-submission-' . $entity->id() . '-locked',
            'title' => $title,
            'class' => ['use-ajax'],
          ],
        ];
      }
      else {
        $render[] = [
          '#type' => 'item',
          '#markup' => WebformSubmissionController::buildLocked($entity),
        ];
      }
    }

    if ($render) {
      $render['#attached']['library'][] = 'webform/webform.admin';
      $render['#attached']['library'][] = 'site_dashboard/site-dashboard';
      $render['#prefix'] = '<div class="request-ajax-field-action">';
      $render['#suffix'] = '</div>';
      $render = Drupal::service('renderer')->render($render);
    }

    return $render ? $render : '';
  }

}
