<?php

namespace Drupal\site_dashboard\Helper;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Helper functions for Site of module.
 *
 * Class SiteDashboardHelper.
 *
 * @package Drupal\site_dashboard\Helper
 */
class SiteDashboardHelper {

  /**
   * Get list services on site.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @return array|mixed
   *  tid => label
   */
  public static function getServices() {
    $services = &drupal_static(__FUNCTION__);
    if (is_null($services)) {
      $services = [];
      $vid = 'application';
      $tree = Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vid);
      foreach ($tree as $item) {
        $services[$item->tid] = $item->name;
      }
    }

    return $services;
  }

  /**
   * Get an service for user.
   *
   * @param null|int $uid
   *
   * @return bool|int
   *   FALSE - if services is not settings user.
   *   int - tid service.
   */
  public static function getServiceByUser($uid = NULL) {
    $data = &drupal_static(__FUNCTION__, []);
    if (is_null($data) || !isset($data[$uid])) {
      $data[$uid] = $account = FALSE;
      if ($uid && is_numeric($uid)) {
        $user = User::load($uid);
        if ($user && $user->hasPermission('site_dashboard access manager')) {
          $account = $user;
        }
      }
      else {
        $user = Drupal::currentUser();
        if ($user && $user->hasPermission('site_dashboard access manager')) {
          $account = User::load($user->id());
        }
      }

      // Get an service from account.
      if ($account
        && $account->hasField(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)
        && !$account->get(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)->isEmpty()
      ) {
        $data[$uid] = $account->get(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)
          ->getString();
      }
    }

    return $data[$uid];
  }

  /**
   * Get status for request.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @return array
   *   tid => name.
   */
  public static function requestGetStatusList() {
    $options = [];
    $vid = 'request_status';
    $tree = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vid);
    foreach ($tree as $item) {
      $options[$item->tid] = $item->name;
    }

    return $options;
  }

  /**
   * Get user type.
   *
   * @param null|string $compare_user_type
   *   Available: guest, administrator, top_manager, sub_administrator, manager.
   * @param null|int $uid
   *   User ID.
   *
   * @return bool|string
   *   string - return type of users.
   *   bool - ifr take $compare_user_type for compare.
   */
  public static function getUserType($compare_user_type = NULL, $uid = NULL) {
    $data = &drupal_static(__FUNCTION__, []);
    $key = $compare_user_type . '|' . $uid;
    if (is_null($data) || !isset($data[$key])) {
      if ($uid && is_numeric($uid)) {
        $user = User::load($uid);
      }
      else {
        $user = Drupal::currentUser();
      }

      $type = 'guest';
      $roles = $user->getRoles();
      if ($compare_user_type == 'sub_administrator' && array_intersect(['sub_administrator', 'Sub Administrator'], $roles)) {
        $type = 'sub_administrator';
      }
      elseif (array_intersect(['administrator', 'sub_administrator'], $roles)) {
        $type = 'administrator';
      }
      elseif (array_intersect(['top_manager', 'Top Manager'], $roles)) {
        $type = 'top_manager';
      }
      elseif (array_intersect(['manager', 'Manager'], $roles)) {
        $type = 'manager';
      }
      $data[$key] = $type;
    }

    if ($compare_user_type) {
      return $data[$key] == $compare_user_type;
    }

    return $data[$key];
  }

  /**
   * Get manager of this job.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Entity node type.
   *
   * @return mixed|Drupal\user\Entity\User
   *   Return user of manager.
   */
  public static function jobGetManager(Node $node) {
    return $node->getOwner();
  }

  /**
   * Get top manager items for an job.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node type job.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]|\Drupal\user\Entity\User[]
   *   Return array users of top manager.
   */
  public static function jobGetTopManagers(Node $node) {
    $managers = [];
    $service_tid = NULL;
    if ($node->hasField(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)
      && !$node->get(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)->isEmpty()
    ) {
      $service_tid = $node->get(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE)
        ->getString();
    }

    if ($service_tid) {
      $user_storage = Drupal::service('entity_type.manager')->getStorage('user');
      $uids = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition(KRUSCHINA_DASHBOARD_FIELD_TAXONOMY_SERVICE, [$service_tid])
        ->condition('roles', 'top_manager')
        ->execute();
      if ($uids) {
        $users = $user_storage->loadMultiple($uids);
        $managers = $users ? $users : [];
      }
    }

    return $managers;
  }

  /**
   * Compare field to change value.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $field_name
   *   Field of entity.
   *
   * @return array|bool
   */
  public static function fieldCompare(EntityInterface $entity, string $field_name) {
    $diff = FALSE;
    if ($entity->hasField($field_name)
      && isset($entity->original)
      && $entity->get($field_name)->value <> $entity->original->get($field_name)->value
    ) {
      $diff = [];
      $diff['new'] = $entity->get($field_name)->value;
      $diff['old'] = $entity->original->get($field_name)->value;
    }

    return $diff;
  }

}
