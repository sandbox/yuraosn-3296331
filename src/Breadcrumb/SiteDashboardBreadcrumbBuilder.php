<?php

namespace Drupal\site_dashboard\Breadcrumb;

use Drupal;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformRequestInterface;

/**
 * Provides a webform breadcrumb builder.
 */
class SiteDashboardBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The current route's entity or plugin type.
   *
   * @var string
   */
  protected $type;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /**
   * Constructs a WebformBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\webform\WebformRequestInterface $request_handler
   *   The webform request handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   */
  public function __construct(ModuleHandlerInterface $module_handler, WebformRequestInterface $request_handler, TranslationInterface $string_translation, ConfigFactoryInterface $config_factory = NULL) {
    $this->moduleHandler = $module_handler;
    $this->requestHandler = $request_handler;
    $this->setStringTranslation($string_translation);
    $this->configFactory = $config_factory ?: Drupal::configFactory();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $apply = FALSE;
    $route_name = $route_match->getRouteName();
    $route_parameters = $route_match->getRawParameters()->all();
    switch ($route_name) {
      case 'entity.webform_submission.edit_form':
      case 'entity.webform_submission.canonical':
        if ($route_parameters['webform'] == 'application_for_job') {
          $request = Drupal::request();
          $webform_submission = $request->get('webform_submission');
          if ($webform_submission) {
            $apply = TRUE;
          }
        }
        break;

      case 'entity.node.edit_form':
        $request = Drupal::request();
        $node = $request->get('node');
        if ($node && $node->bundle() == 'job_directory') {
          $apply = TRUE;
        }
        break;
    }

    return $apply;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    $request = Drupal::request();
    $title = t('Dashboard');

    switch ($route_name) {
      case 'entity.webform_submission.edit_form':
      case 'entity.webform_submission.canonical':
        $path = 'admin/dashboard/request';
        break;

      case 'entity.node.edit_form':
      default:
        $path = 'admin/people';
    }

    $destination = $request->query->get('destination');
    if ($destination && strpos($destination, '/admin/dashboard') !== FALSE) {
      $path = $destination;
    }
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));
    $link = Link::fromTextAndUrl($title, Url::fromUri('base:' . $path));
    $breadcrumb->addLink($link);

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }

}
