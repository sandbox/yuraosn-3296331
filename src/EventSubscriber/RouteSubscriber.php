<?php

namespace Drupal\site_dashboard\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\site_dashboard\EventSubscriber
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {

    /** @var \Symfony\Component\Routing\Router $route */
    if ($route = $collection->get('entity.webform_submission.notes_form')) {
      $route->setRequirement('_entity_access', 'webform_submission.update');
    }

    /** @var \Symfony\Component\Routing\Router $route */
    if ($route = $collection->get('entity.webform_submission.clone_form')) {
      $route->setRequirement('_access', 'FALSE');
    }

    /** @var \Symfony\Component\Routing\Router $route */
    if ($route = $collection->get('entity.webform_submission.duplicate_form')) {
      $route->setRequirement('_access', 'FALSE');
    }

  }

}
