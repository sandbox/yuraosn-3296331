<?php

namespace Drupal\site_dashboard\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_dashboard\Helper\SiteDashboardRequestHelper;

/**
 * Class RequestForm.
 *
 * @package Drupal\site_dashboard\Form
 */
class RequestForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'site_dashboard_request_view_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $webform_submission = NULL) {

    $form['#prefix'] = '<div id="site_dashboard-request-form-wrapper">';
    $form['#suffix'] = '</div>';

    if ($webform_submission) {
      $form_tmp = SiteDashboardRequestHelper::actionsFormBuild($webform_submission, 'edit');
      if ($form_tmp) {
        $form += $form_tmp;
      }
    }

    return $form;
  }

  /**
   * Ajax callback for the status field.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function statusAjaxCallback(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();

    /** @var \Drupal\webform\Entity\WebformSubmission $webform_submission */
    $webform_submission = $build_info['args'][0];

    $status_origin = $webform_submission->getElementData('status');
    $status = $form_state->getValue('status');
    if ($status_origin <> $status) {
      $webform_submission->setElementData('status', $status);
      $webform_submission->save();
    }

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#site-dashboard-request-view-form .form-item-status .status', $this->t('Saved')));

    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
