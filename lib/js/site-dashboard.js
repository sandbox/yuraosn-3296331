(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.jobAdmin = {
    attach: function (context, settings) {

      $('.ajax-progress-fullscreen').remove();

      //load select2
      if (context === document && typeof jQuery.fn.select2 == 'undefined') {
        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css';
        document.head.appendChild(link);

        var script = document.createElement('script');
        script.src = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js';
        document.body.appendChild(script);
        script.onload = function () {

          $('select[multiple]').select2({
            multiple: true,
            placeholder: 'wähle eine Option',
            allowClear: true,
          });

        }
      } else {
        if (typeof jQuery.fn.select2 != 'undefined') {
          $('.select2-container--open').remove();
          $('select[multiple]').select2({
            multiple: true,
            placeholder: 'wähle eine Option',
            allowClear: true,
          });
        }
      }

      //rate it
      if (context !== document) {
        $(context)
          .find('[data-rateit-readonly]')
          .each(function () {
            var $rateit = $(this);
            if (!$.fn.rateit) {
              $rateit.remove();
              return;
            }

            // Rateit only initialize inputs on load.
            if (document.readyState === 'complete') {
              $rateit.rateit();
            } else {
              window.setTimeout(function () {
                $rateit.rateit();
              });
            }
          });
      }

      //tooltip destroy
      var i = 0;
      var cb = setInterval(function () {
        if ($(".ui-tooltip").length) {
          $('.js-webform-element-help')
            .tooltip('close')
            .blur();
          clearTimeout(cb);
        }

        // if too long
        i += 100;
        if (i === 2000) {
          clearTimeout(cb);
        }
      }, 100);
    }
  };
}(jQuery, Drupal));
